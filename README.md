Essa tese consiste em investigar e determinar uma fórmula explícita para a
*q-massa Gauss-Bonnet-Chern* na *classe das subvariedades gráficas Euclidianas
assintoticamente planas*. Fazemos uso dessa fórmula para resolver parcialmente
as *conjecturas da massa positiva* e *de Penrose* para esse conceito de massa (nessa
classe de variedades).